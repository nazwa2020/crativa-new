<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pricing extends Model
{
    use HasFactory;

    protected $table = 'pricing';
    protected $primaryKey = 'id_pricing';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $guarded = [];

    public function privillege() {
        return $this->HasMany(privillege::class, 'id_pricing', 'id_pricing');
    }
}
