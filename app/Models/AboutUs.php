<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    use HasFactory;
    protected $table = 'about_us';
    protected $primaryKey = 'id_about';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $guarded = [];
}
