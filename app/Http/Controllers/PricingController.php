<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\pricing;
use App\Models\privillege;
use Illuminate\Http\Request;

class PricingController extends Controller
{
    public function pricing()
    {
        $pricing = pricing::get();
        return view('admin.pricing.index', compact('pricing'));
    }

    public function add()
    {
        $pricing = null;
        return view("admin.pricing.action", compact('pricing'));
    }

    public function edit(pricing $pricing)
    {
        $pricing = $pricing;
        return view("admin.pricing.action", compact('pricing'));
    }

    public function addPrivillege($id_pricing)
    {
        $pricing = pricing::find($id_pricing);
        return view("admin.pricing.addPrivillege", compact('pricing'));
    }

    public function storePrivillege( Request $request)
    {
        // $request->validate([
        //     'id_pricing' => 'required|unique:pricing,id_pricing',
        //     'categories' => 'required',
        //     'price' => 'required',
        //     'feature' => 'required',
        // ], [
        //     'id_pricing.required' => 'ID pricing harus diisi.',
        //     'id_pricing.unique' => 'ID pricing has been used.',
        //     'categories.required' => 'Judul pricing harus diisi.',
        //     'price.required' => 'Informasi pricing harus diisi.',
        //     'feature.required' => 'Feature pricing harus diisi.',
        // ]);

        privillege::create([
            'id_pricing' => $request->id_pricing,
            'privillege' => $request->privillege
        ]);

        // return redirect()->route('pricing.pricing');
        // Dalam kontroler Anda
        return redirect()->route('pricing.pricing')->with('success', 'Data berhasil ditambahkan!');

    }

    public function store(Request $request)
    {
        $request->validate([
            'id_pricing' => 'required|unique:pricing,id_pricing',
            'categories' => 'required',
            'price' => 'required',
        ], [
            'id_pricing.required' => 'ID pricing harus diisi.',
            'id_pricing.unique' => 'ID pricing has been used.',
            'categories.required' => 'Judul pricing harus diisi.',
            'price.required' => 'Informasi pricing harus diisi.',
        ]);

        pricing::create([
            'id_pricing' => $request->id_pricing,
            'categories' => $request->categories,
            'price' => $request->price,
        ]);

        // return redirect()->route('pricing.pricing');
        // Dalam kontroler Anda
        return redirect()->route('pricing.pricing')->with('success', 'Data berhasil ditambahkan!');

    }

    public function update(Request $request, pricing $pricing)
    {
        $request->validate([
            'id_pricing' => 'required',
            'categories' => 'required',
            'price' => 'required',
        ], [
            'id_pricing.required' => 'ID pricing harus diisi.',
            'categories.required' => 'Judul pricing harus diisi.',
            'price.required' => 'Informasi pricing harus diisi.',
        ]);

        $pricing->update([
            'id_pricing' => $request->id_pricing,
            'categories' => $request->categories,
            'price' => $request->price,
        ]);

        // return redirect()->route('pricing.pricing');
        return redirect()->route('pricing.pricing')->with('success', 'Data berhasil diubah!');

    }

    public function delete (pricing $pricing)
    {
            // $pricing parameter sudah berisi instansiasi snackbox berdasarkan id_pricing
    
        if (!$pricing) {
            // Jika pricing tidak ditemukan, mungkin hendak menampilkan pesan error atau melakukan tindakan lain
            return redirect('/pricing')->with('error', 'pricing not found');
        }
    
        // Hapus record pricing dari database
        $pricing->delete();
    
        // Redirect ke halaman yang sesuai atau tampilkan pesan sukses
        return redirect('/pricing')->with('success', 'pricing deleted successfully');
    }
}
