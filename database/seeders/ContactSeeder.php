<?php

namespace Database\Seeders;

use App\Models\contact;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        contact::create([
            'id_contact' => 'C01',
            'contact_cat' => 'Phone',
            'information' => '+62 22 1234 56789'
        ]);

        contact::create([
            'id_contact' => 'C02',
            'contact_cat' => 'Email',
            'information' => 'crativakitchen@mail.com'
        ]);

        contact::create([
            'id_contact' => 'C03',
            'contact_cat' => 'Location',
            'information' => 'Jakarta, Indonesia'
        ]);
    }
}
