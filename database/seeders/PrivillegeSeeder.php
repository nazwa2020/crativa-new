<?php

namespace Database\Seeders;

use App\Models\privillege;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PrivillegeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        privillege::create([
            'id_pricing' => 'PR01',
            'privillege' => 'Up to 4 connection'
        ]);

        privillege::create([
            'id_pricing' => 'PR02',
            'privillege' => 'Up to 6 connection'
        ]);

        privillege::create([
            'id_pricing' => 'PR02',
            'privillege' => 'Free installation'
        ]);

        privillege::create([
            'id_pricing' => 'PR02',
            'privillege' => 'Special meals every 1 week'
        ]);

        privillege::create([
            'id_pricing' => 'PR03',
            'privillege' => 'Up to 12 connection'
        ]);

        privillege::create([
            'id_pricing' => 'PR03',
            'privillege' => 'Free installation'
        ]);

        privillege::create([
            'id_pricing' => 'PR03',
            'privillege' => 'Special people every 1 week'
        ]);

        privillege::create([
            'id_pricing' => 'PR03',
            'privillege' => 'Free futurism'
        ]);
    }
}
