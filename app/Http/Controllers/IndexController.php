<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use App\Models\contact;
use App\Models\features;
use App\Models\information;
use App\Models\pricing;
use App\Models\photos;
use App\Models\privillege;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $pricing = pricing::get();
        // $pricing = pricing::join('privillege', 'privillege.id_pricing', '=', 'pricing.id_pricing')
        // ->select('privillege.*', 'pricing.*')->get();
        $feature = features::get();
        $contact = contact::get();
        $about = AboutUs::get();
        $privillege = privillege::get();
        return view('index', compact('feature', 'pricing', 'contact', 'about', 'privillege'));
    }

    
}
