<?php

namespace Database\Seeders;

use App\Models\photos;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PhotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        photos::create([
            'id_photo' => 'PH01',
            'name' => 'Banner-1',
            'url' => 'banner-1.jpeg'
        ]);

        photos::create([
            'id_photo' => 'PH02',
            'name' => 'Banner-2',
            'url' => 'banner-4.jpeg'
        ]);

        photos::create([
            'id_photo' => 'PH03',
            'name' => 'Info-1',
            'url' => 'banner-3.jpeg'
        ]);

        photos::create([
            'id_photo' => 'PH04',
            'name' => 'Info-2',
            'url' => 'banner-5.jpeg'
        ]);
    }
}
