<!DOCTYPE html>
<html lang="en">
@include('layouts.head')

<body>
    <div class="page-wrapper">
        @include('layouts.header')
        @yield('content')
        @include('layouts.footer')
    </div>
    @include('layouts.script')
    @include('layouts.foot')
</body>

</html>