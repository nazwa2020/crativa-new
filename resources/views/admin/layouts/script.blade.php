<!-- plugins:js -->
<script src="{{ asset('asset/vendors/js/vendor.bundle.base.js') }}"></script>
<script src="{{ asset('asset/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="{{ asset('asset/vendors/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('asset/vendors/progressbar.js/progressbar.min.js') }}"></script>
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="{{ asset('asset/js/off-canvas.js') }}"></script>
<script src="{{ asset('asset/js/hoverable-collapse.js') }}"></script>
<script src="{{ asset('asset/js/template.js') }}"></script>
<script src="{{ asset('asset/js/settings.js') }}"></script>
<script src="{{ asset('asset/js/todolist.js') }}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{ asset('asset/js/jquery.cookie.js') }}" type="text/javascript"></script>
<script src="{{ asset('asset/js/dashboard.js') }}"></script>
<script src="{{ asset('asset/js/proBanner.js') }}"></script>
<script src="js/app.js"></script>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<!-- <script src="../../asset/js/Chart.roundedBarCharts.js"></script> -->
<!-- End custom js for this page-->