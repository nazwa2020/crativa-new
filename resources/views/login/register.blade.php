<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="loginassets/fonts/icomoon/style.css">

    <link rel="stylesheet" href="loginassets/css/owl.carousel.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="loginassets/css/bootstrap.min.css">

    <!-- Style -->
    <link rel="stylesheet" href="loginassets/css/style.css">

    <title>Login #7</title>
</head>

<body>



    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="loginassets/images/undraw_remotely_2j6y.svg" alt="Image" class="img-fluid">
                </div>
                <div class="col-md-6 contents">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <div class="mb-4">
                                <h3>Sign Up First!</h3>

                            </div>
                            <form action="/register/store" method="post">
                                {{ csrf_field() }}
                                
                                <label for="username">Username</label>
                                <div class="form-group first mb-2">
                                    <input type="text" class="form-control" id="username" name="username"
                                        value="{{ Session::get('username') }}">
                                    
                                </div>
                                @error('username')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                
                                <label for="Email">Email</label>
                                <div class="form-group last mb-2">
                                    <input type="email" class="form-control" id="email" name="email"
                                        value="{{ Session::get('email') }}">
                                    
                                </div>
                                @error('email')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                
                                <label for="password">Password</label>
                                <div class="form-group last mb-2">
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                        id="password" name="password" autocomplete="current-password">
                                </div>
                                @error('password')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror

                                <label for="password_confirmation">Confirm Password</label>
                                <div class="form-group last mb-2">
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                        id="password" autocomplete="current-password">
                                </div>

                                <div class="d-flex mb-4 align-items-center">
                                    <span class="ml-auto"><a href="/login" class="forgot-pass">Have an Account? Login
                                            Now!</a></span>
                                </div>

                                <input type="submit" value="Sign Up" class="btn btn-block btn-primary">

                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>


    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
