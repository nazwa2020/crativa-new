<!DOCTYPE html>
<html lang="en">

@include('admin.layouts.head')

<body>
    <div class="container-scroller">
        <!-- partial:../../partials/_navbar.html -->
        @include('admin.layouts.navbar')
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:../../partials/_settings-panel.html -->
            <div class="theme-setting-wrapper">
                <div id="settings-trigger"><i class="ti-settings"></i></div>
                <div id="theme-settings" class="settings-panel">
                    <i class="settings-close ti-close"></i>
                    <p class="settings-heading">SIDEBAR SKINS</p>
                    <div class="sidebar-bg-options selected" id="sidebar-light-theme">
                        <div class="img-ss rounded-circle bg-light border me-3"></div>Light
                    </div>
                    <div class="sidebar-bg-options" id="sidebar-dark-theme">
                        <div class="img-ss rounded-circle bg-dark border me-3"></div>Dark
                    </div>
                    <p class="settings-heading mt-2">HEADER SKINS</p>
                    <div class="color-tiles mx-0 px-4">
                        <div class="tiles success"></div>
                        <div class="tiles warning"></div>
                        <div class="tiles danger"></div>
                        <div class="tiles info"></div>
                        <div class="tiles dark"></div>
                        <div class="tiles default"></div>
                    </div>
                </div>
            </div>
            
            <!-- partial -->
            <!-- partial:../../partials/_sidebar.html -->
            @include('admin.layouts.sidebar')
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-md-11 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h3 class="card-title">Form Data Fitur</h3>
                                    <form method="post"
                                        action="{{ $feature ? route('feature.update', $feature->id_feature) : route('feature.store') }}"
                                        enctype="multipart/form-data" class="forms-sample">
                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label for="exampleInputUsername1">ID Feature</label>
                                            <input type="number" class="form-control" id="exampleInputUsername1" name="id_feature"
                                                placeholder="ID Feature" value="{{ $feature ? $feature->id_feature : old('id_feature') }}">
                                            @if ($errors->has('id_feature'))
                                                <div class="text-danger">
                                                    {{ $errors->first('id_feature') }}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nama Fitur</label>
                                            <input type="text" class="form-control" id="exampleInputEmail1" name="title"
                                                placeholder="Nama Fitur" value="{{ $feature ? $feature->title : old('title') }}">
                                            @if ($errors->has('title'))
                                                <div class="text-danger">
                                                    {{ $errors->first('title') }}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Informasi</label>
                                            <input type="text" class="form-control" id="exampleInputPassword1" name="information"
                                                placeholder="Informasi" value="{{ $feature ? $feature->information : old('information') }}">
                                            @if ($errors->has('information'))
                                                <div class="text-danger">
                                                    {{ $errors->first('information') }}
                                                </div>
                                            @endif
                                        </div>
                                        <button type="submit" class="btn btn-primary me-2">Submit</button>
                                        <button class="btn btn-light">Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- content-wrapper ends -->
                <!-- partial:../../partials/_footer.html -->
                @include('admin.layouts.footer')
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.layouts.script')
</body>

</html>
