<?php

namespace Database\Seeders;

use App\Models\information;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class InfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        information::create([
            'id_info' => 'IN01',
            'title' => 'Technology for the people future',
            'information' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. 
            Eligendi blanditiis sapiente non excepturi.'
        ]);

        information::create([
            'id_info' => 'IN02',
            'title' => 'We specialize take over people power to enhance futurism mind.'
        ]);

        information::create([
            'id_info' => 'IN03',
            'title' => 'Meals that you have to serve before work, study and meeting with friends together. All in your hand.'
        ]);

        information::create([
            'id_info' => 'IN04',
            'title' => 'Launch play mode and tell your friends',
            'information' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
            Repellat a fuga corrupti inventore officia porro nulla omnis molestiae doloremque nihil officiis ex, nulla laborum voluptatibus ab maxime reprehenderit accusantium. Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
        ]);

        information::create([
            'id_info' => 'IN05',
            'title' => 'Launch play mode with your family',
            'information' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat a fuga corrupti inventore officia porro nulla omnis molestiae doloremque nihil officiis ex, nulla laborum voluptatibus ab maxime reprehenderit accusantium. Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
        ]);

        information::create([
            'id_info' => 'IN06',
            'title' => 'Get the future people power free access now',
            'information' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam iure perferendis molestias tempore.'
        ]);
    }
}
