
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="/admin">
          <i class="mdi mdi-grid-large menu-icon"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      
      <li class="nav-item nav-category">Forms and Datas</li>
      
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#charts" aria-expanded="false" aria-controls="charts">
          <i class="menu-icon mdi mdi-table"></i>
          <span class="menu-title">Tables RU</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="charts">
          <ul class="nav flex-column sub-menu ">
            <li class="nav-item"> <a class="nav-link" href="/info">Info</a></li>
          </ul>
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="/contact">Kontak</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#tables" aria-expanded="false" aria-controls="tables">
          <i class="menu-icon mdi mdi-table"></i>
          <span class="menu-title">Tables CRUD</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="tables">
         <ul class="nav flex-column sub-menu" style="border-radius: 1px;">
            <li class="nav-item"> <a class="nav-link" href="/feature">Data Fitur</a></li>
          </ul>
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="/pricing">Data Pricing</a></li>
          </ul>
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="/privillege">Data Privillege</a></li>
          </ul>
        </div>
      </li>
      
      
      
    </ul>
  </nav>