<!DOCTYPE html>
<html lang="en">

@include('admin.layouts.head')

<body>
  <div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->
    @include('admin.layouts.navbar')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:../../partials/_settings-panel.html -->
      <div class="theme-setting-wrapper">
  <div id="settings-trigger"><i class="ti-settings"></i></div>
  <div id="theme-settings" class="settings-panel">
    <i class="settings-close ti-close"></i>
    <p class="settings-heading">SIDEBAR SKINS</p>
    <div class="sidebar-bg-options selected" id="sidebar-light-theme">
      <div class="img-ss rounded-circle bg-light border me-3"></div>Light
    </div>
    <div class="sidebar-bg-options" id="sidebar-dark-theme">
      <div class="img-ss rounded-circle bg-dark border me-3"></div>Dark
    </div>
    <p class="settings-heading mt-2">HEADER SKINS</p>
    <div class="color-tiles mx-0 px-4">
      <div class="tiles success"></div>
      <div class="tiles warning"></div>
      <div class="tiles danger"></div>
      <div class="tiles info"></div>
      <div class="tiles dark"></div>
      <div class="tiles default"></div>
    </div>
  </div>
</div>

      <!-- partial -->
      <!-- partial:../../partials/_sidebar.html -->
      @include('admin.layouts.sidebar')
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="card-body d-flex justify-content-between align-items-center">
                    <h4 class="card-title">Data Fitur</h4>
                    <div class="text-right">
                        <a href="{{ route('feature.add') }}" class="btn btn-primary" data-toggle=""
                            style="border-radius: 7px; margin-left:50px;">
                            <span><i class="fas fa-plus mx-1"></i>  Tambah Fitur</span>
                        </a>
                    </div>
                </div>
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                            ID Fitur
                          </th>
                          <th>
                            Nama Fitur
                          </th>
                          <th width="100px">
                            Informasi
                          </th>
                          <th >
                            Action
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($feature as $item)
                        <tr>
                          <td>
                            <b>{{$item->id_feature}}</b>
                          </td>
                          <td>
                            {{$item->title}}
                          </td>
                          <td>
                            {{$item->information}}
                          </td>
                          <td>
                            <a href="{{ route('feature.edit' , $item->id_feature) }}" class="btn btn-primary" data-toggle=""
                            style="border-radius: 7px;">
                            <span><i class="fas fa-edit"></i></span>
                          </a>
                          <a href="{{ route('feature.delete' , $item->id_feature) }}" class="btn btn-primary" data-toggle=""
                            style="border-radius: 7px;">
                            <span><i class="fas fa-trash-alt"></i></span>
                          </a>
                          </td>
                        </tr> 
                        @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        @include('admin.layouts.footer')
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  @include('admin.layouts.script')
</body>

</html>