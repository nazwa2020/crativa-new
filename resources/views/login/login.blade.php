<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="loginassets/fonts/icomoon/style.css">

    <link rel="stylesheet" href="loginassets/css/owl.carousel.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="loginassets/css/bootstrap.min.css">

    <!-- Style -->
    <link rel="stylesheet" href="loginassets/css/style.css">

    <title>Login #7</title>
</head>

<body>



    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="loginassets/images/undraw_remotely_2j6y.svg" alt="Image" class="img-fluid">
                </div>
                <div class="col-md-6 contents">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <div class="mb-4">
                                <h3>Sign In Now!</h3>

                            </div>
                            <form action="/login/action" method="post">
                                {{ csrf_field() }}
                                <center>
                                    @if (session('error'))
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert"
                                            style="padding: 0.3rem; font-size: 0.9rem;">
                                            {{ session('error') }}
                                        </div>
                                    @endif
                                </center>

                                <label for="Email">Email</label>
                                <div class="form-group first mb-2">
                                    <input type="email" class="form-control" id="email" name="email"
                                        value="{{ Session::get('email') }}">
                                </div>

                                <label for="password">Password</label>
                                <div class="form-group last mb-2">
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                        id="password" name="password" autocomplete="current-password">
                                </div>

                                <div class="d-flex mb-4 align-items-center">
                                    <span class="ml-auto"><a href="#" class="forgot-pass">Forgot
                                            Password</a></span>
                                </div>

                                <input type="submit" value="Sign In" class="btn btn-block btn-primary mb-2">
                                <div class="d-flex mb-5 align-items-center">
                                    <a href="/register" class="forgot-pass">Belum punya akun? Register Sekarang!</a>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>


    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
