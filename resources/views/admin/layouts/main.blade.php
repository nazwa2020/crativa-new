<!DOCTYPE html>
<html lang="en">
@include('admin.layouts.head')

<body>
    <div class="page-wrapper">
        @yield('content')
    </div>
    @include('admin.layouts.script')
</body>

</html>