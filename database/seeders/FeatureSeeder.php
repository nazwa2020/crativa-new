<?php

namespace Database\Seeders;

use App\Models\features;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        features::create([
            'id_feature' => '01',
            'title' => 'Break the rules on the go anytime flawless',
            'information' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            Laborum laudantium sitatibus voluptates optioulla.'
        ]);

        features::create([
            'id_feature' => '02',
            'title' => 'Install the connection everywhere',
            'information' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            Laborum laudantium sitatibus voluptates optioulla.'
        ]);

        features::create([
            'id_feature' => '03',
            'title' => 'Launch play mode and tell your friends',
            'information' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            Laborum laudantium sitatibus voluptates optioulla.'
        ]);
    }
}
