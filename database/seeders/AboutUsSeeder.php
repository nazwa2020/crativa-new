<?php

namespace Database\Seeders;

use App\Models\AboutUs;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AboutUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        AboutUs::create([
            'id_about' => '01',
            'title' => 'Technology for the people future',
            'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eligendi blanditiis sapiente non excepturi.',
            'url' => 'banner-1.jpeg'
        ]);

        AboutUs::create([
            'id_about' => '02',
            'title' => 'We specialize take over people power to enhance futurism mind.',
            'url' => 'banner-4.jpeg'
        ]);

        AboutUs::create([
            'id_about' => '03',
            'title' => 'Meals that you have to serve before work, study and meeting with friends together. All in your hand.'
        ]);

        AboutUs::create([
            'id_about' => '04',
            'title' => 'Launch play mode and tell your friends',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat a fuga 
            corrupti inventore officia porro nulla omnis molestiae doloremque nihil officiis ex, nulla 
            laborum voluptatibus ab maxime reprehenderit accusantium. Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
            'url' => 'banner-3.jpeg'
        ]);

        AboutUs::create([
            'id_about' => '05',
            'title' => 'Get the future people power free access now',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam iure perferendis molestias tempore.',
            'url' => 'banner-5.jpeg'
        ]);

        AboutUs::create([
            'id_about' => '06',
            'title' => 'Launch play mode with your family',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat a fuga corrupti 
            inventore officia porro nulla omnis molestiae doloremque nihil officiis ex, nulla laborum voluptatibus 
            ab maxime reprehenderit accusantium. Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
        ]);
    }
}
