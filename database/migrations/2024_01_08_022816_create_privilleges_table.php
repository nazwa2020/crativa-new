<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('privillege', function (Blueprint $table) {
            $table->id();
            $table->string('id_pricing');
            $table->string('privillege');
            $table->timestamps();

            $table->foreign('id_pricing')->references('id_pricing')->on('pricing')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('privilleges');
    }
};
