<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class features extends Model
{
    use HasFactory;

    protected $table = 'feature';
    protected $primaryKey = 'id_feature';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $guarded = [];
}
