<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class information extends Model
{
    use HasFactory;

    protected $table = 'information';
    protected $primaryKey = 'id_info';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $guarded = [];
}
