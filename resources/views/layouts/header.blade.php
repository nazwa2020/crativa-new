<div class="page-wrappper">
    <!-- Navigation -->
    <div class="nav-top px-lg-4 px-2">
        <div class="nav-inner h-100">
            <div class="container-fluid h-100">
                <div class="row h-100">
                    <div class="col-4 h-100">
                        <div class="d-flex align-items-center h-100">
                            <div class="nav-top-menus nav-top-menus--white">
                                <a href="#featuresSection" class="menus-link me-3">Features</a>
                                <a href="#pricingSection" class="menus-link me-3">Pricing</a>
                                <a href="#contactSection" class="menus-link">Contact</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 h-100">
                        <div class="d-flex justify-content-center align-items-center h-100">
                            <a href="#" class="nav-logo nav-logo-lg text-white">
                                Crativa
                            </a>
                        </div>
                    </div>
                    <div class="col-4 h-100">
                        <div class="d-flex align-items-center justify-content-end h-100">
                            <div class="nav-top-menus nav-top-menus--white">
                                <a href="#" class="menus-link me-3"><i class="icofont-instagram"></i></a>
                                <a href="#" class="menus-link"><i class="icofont-twitter"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>