<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\privillege;
use Illuminate\Http\Request;

class PrivillegeController extends Controller
{
    public function privillege()
    {
        $privillege = privillege::get();
        return view("admin.privillege.index", compact('privillege'));
    }
    public function editPrivillege($id)
    {
        $privillege = privillege::find($id);
        return view("admin.privillege.action", compact('privillege'));
    }

    public function updatePrivillege(Request $request, $id)
    {
        $request->validate([
            'id_pricing' => 'required',
            'privillege' => 'required'
        ], [
            'id_pricing.required' => 'ID pricing harus diisi.',
            'privillege.required' => 'Judul privillege harus diisi.'
        ]);

        privillege::find($id)->update([
            'id_pricing' => $request->id_pricing,
            'privillege' => $request->privillege
        ]);

        // return redirect()->route('feature.feature');
        return redirect('/privillege')->with('success', 'Data berhasil diubah!');

    }

    public function delete($id)
    {
        $privillege = privillege::find($id);
        
        $privillege->delete();
        return redirect('/privillege')->with('success', 'Data berhasil dihapus!');
    }
}
