<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use App\Models\contact;
use App\Models\information;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ReadUpdateController extends Controller
{
    public function info()
    {
        $info = AboutUs::get();
        return view('admin.info.index', compact('info'));
    }

    public function coba()
    {
        $about = AboutUs::get();
        return view('admin.info.index', compact('about'));
    }

    public function editInfo(AboutUs $info)
    {
        $info = $info;
        return view("admin.info.action", compact('info'));
    }

    public function updateInfo(Request $request, AboutUs $info)
    {
        $request->validate([
            'id_about' => 'required',
            'title' => 'required'
        ], [
            'id_about.required' => 'ID  harus diisi.',
            'title.required' => 'Judul Header harus diisi.'
        ]);

        $info = AboutUs::find($request->id_about);

        if ($info) {
            $info->update([
                'id_about' => $request->id_about,
                'title' => $request->title,
                'description' => $request->description
            ]);

            if ($request->hasFile('url')) {
                $request->validate([
                    'url' => 'mimes:jpeg,jpg,png,gif'
                ]);

                $foto_file = $request->file('url');
                $foto_ekstensi = $foto_file->extension();
                $foto_nama = date('ymdhis') . "." . $foto_ekstensi;
                $foto_file->move(public_path('assets/img'), $foto_nama);

                $old_url = $info->url;

                $info->update([
                    'url' => $foto_nama
                ]);

                // Hapus file foto lama setelah berhasil mengupdate yang baru
                File::delete(public_path('assets/img') . '/' . $old_url);
            }
        }
        return redirect()->route('info.info')->with('success', 'Data berhasil diubah!');
    }

    public function contact()
    {
        $contact = contact::get();
        return view('admin.contact.index', compact('contact'));
    }

    public function editContact(contact $contact)
    {
        $contact = $contact;
        return view("admin.contact.action", compact('contact'));
    }

    public function updateContact(Request $request, contact $contact)
    {
        $request->validate([
            'id_contact' => 'required',
            'contact_cat' => 'required',
            'information' => 'required'
        ], [
            'id_contact.required' => 'ID Kontak harus diisi.',
            'contact_cat.required' => 'Jenis Kontak harus diisi.',
            'information.required' => 'Informasi harus diisi.'
        ]);

        $contact->update([
            'id_contact' => $request->id_contact,
            'contact_cat' => $request->contact_cat,
            'information' => $request->information
        ]);

        // return redirect()->route('info.info');
        return redirect()->route('contact.contact')->with('success', 'Data berhasil diubah!');
    }

    public function photo()
    {
        return view('admin.photo.index');
    }
}
