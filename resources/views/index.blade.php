@extends('layouts.main')

@section('content')

        @php
        $about1 = $about->where('id_about','01')->first();
        $about2 = $about->where('id_about','02')->first();
        $about3 = $about->where('id_about','03')->first();
        $about4 = $about->where('id_about','04')->first();
        $about5 = $about->where('id_about','05')->first();
        $about6 = $about->where('id_about','06')->first();
        @endphp
        <!-- Section > Home Banner -->
        
        <div class="section-offset-nav d-block py-lg-5 py-3 bg-images border-bottom border-thick border-secondary" style="background-image: url({{ asset('assets/img/' . $about1->url) }});">
            <div class="section-have-cover container my-lg-5 my-4 px-lg-5">
                <div class="row align-items-center">
                    <div class="col-md-7 mb-lg-5 mb-4 text-white">
                        <h1 class="mb-3 text-xlg">{{$about1->title}}</h1>
                        <p class="mb-4 font-regular pe-lg-5">{{$about1->description}}</p>
                        <a href="/register" class="btn btn-lg btn-primary px-4">Sign Up <i class="icofont-arrow-right ms-3"></i></a>
                    </div>
                </div>
            </div>
            <div class="bg-images-cover"></div>
        </div>
        
        
        <div id="featuresSection" class="d-block bg-white py-lg-5 py-3">
            <div class="container my-lg-5 my-4 px-lg-5 text-center">
                <div class="font-weight-bold text-lg d-block mb-4">
                    {{$about2->title}}
                </div>
            </div>
        </div>
        <div class="d-block section-xlg bg-images" style="background-image: url({{ asset('assets/img/' . $about2->url) }});"></div>
        
        
        <!-- Section > Feature -->
        <div id="featuresSection" class="d-block bg-secondary py-lg-5 py-3">
            <div class="container my-lg-5 my-4 px-lg-5">
                <h2 class="mb-lg-5 mb-4">Features</h2>
                <div class="row">
                    @foreach ($feature as $item)
                    <div class="col-md-4 mb-lg-3 mb-3">
                        <div class="d-flex flex-column pb-3 mb-3 border-bottom border-primary h-100">
                            <div class="text-lg font-thin text-primary d-block mb-3">#{{$item->id_feature}}</div>
                            <div class="flex-1">
                                <h4 class="mb-3">{{$item->title}}</h4>
                                <p class="small">{{$item->information}}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- Section -->
        
        <div class="d-block bg-dark text-white py-lg-5 py-3">
            <div class="container my-lg-5 my-4 px-lg-5 text-center">
                <div class="d-block text-lg font-thin mb-5">
                    {{$about3->title}}
                </div>
            </div>
        </div>
        
        
        <!-- Section > Content -->
        <div class="d-block py-lg-5 py-3">
            <div class="container my-lg-5 my-4 px-lg-5">
                <div class="row align-items-center">
                    <div class="col-md-6 order-md-2 mb-lg-0 mb-4">
                        <div class="block-img rounded" style="background-image: url({{ asset('assets/img/' . $about4->url) }});"></div>
                    </div>
                    <div class="col-md-6 order-md-1 mb-lg-0 mb-4">
                        <div class="d-block pe-lg-5">
                            <h2 class="mb-3">{{$about4->title}}</h2>
                            <p class="small m-0">{{$about4->description}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Section > Content -->
        
        <div class="d-block py-lg-5">
            <div class="container my-lg-5 my-4 px-lg-5">
                <div class="row align-items-center">
                    <div class="col-md-6 mb-lg-0 mb-3">
                        <div class="block-img rounded" style="background-image: url({{ asset('assets/img/' . $about5->url) }});"></div>
                    </div>
                    <div class="col-md-6 mb-lg-0 mb-3">
                        <div class="d-block ps-lg-5">
                            <h2 class="mb-3">{{$about5->title}}</h2>
                            <p class="small m-0">{{$about5->description}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Section > Pricing -->
        <div id="pricingSection" class="d-block py-lg-5 py-3 bg-dark">
            <div class="container my-lg-5 my-4 px-lg-5">
                <h2 class="mb-lg-5 mb-4 text-white">Pricing</h2>
                <div class="row">
                    @foreach ($pricing as $item)
                    <div class="col-md-4 mb-lg-3 mb-4">
                        <div class="card border-0 h-100">
                            <div class="card-body">
                                <div class="d-flex flex-column p-lg-3 h-100">
                                    <div class="flex-1 mb-4">
                                        <h5>{{$item->categories}}</h5>
                                        <div class="d-block text-lg font-thin">
                                            ${{$item->price}}/month
                                        </div>
                                        <ul class="list-group list-group-flush">
                                        @foreach ($item->privillege as $item)
                                            <li class="list-group-item border-bottom px-0">{{$item->privillege}}</li>
                                        @endforeach
                                    </ul> 
                                    </div>
                                    <div class="d-block w-100 text-center">
                                        <a href="#" class="btn btn-primary d-block mb-2">Choose This Plan</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
        <div class="d-block bg-light py-lg-5 py-3">
            <div class="container my-lg-5 my-4 px-lg-5">
                <div class="row justify-content-center">
                    <div class="col-md-7">
                        <div class="d-block text-center">
                            <h1 class="mb-3">{{$about6->title}}</h1>
                            <p class="font-regular">{{$about6->description}}</p>
                            <a href="#" class="btn btn-primary btn-lg px-4">Signup Now <i class="icofont-arrow-right ms-3"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
@endsection