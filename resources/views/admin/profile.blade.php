<!DOCTYPE html>
<html lang="en">

@include('admin.layouts.head')

<body class="with-welcome-text">
    <div class="container-scroller">
        @include('admin.layouts.navbar')
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:partials/_settings-panel.html -->
            <div class="theme-setting-wrapper">
                <div id="settings-trigger"><i class="ti-settings"></i></div>
                <div id="theme-settings" class="settings-panel">
                    <i class="settings-close ti-close"></i>
                    <p class="settings-heading">SIDEBAR SKINS</p>
                    <div class="sidebar-bg-options selected" id="sidebar-light-theme">
                        <div class="img-ss rounded-circle bg-light border me-3"></div>Light
                    </div>
                    <div class="sidebar-bg-options" id="sidebar-dark-theme">
                        <div class="img-ss rounded-circle bg-dark border me-3"></div>Dark
                    </div>
                    <p class="settings-heading mt-2">HEADER SKINS</p>
                    <div class="color-tiles mx-0 px-4">
                        <div class="tiles success"></div>
                        <div class="tiles warning"></div>
                        <div class="tiles danger"></div>
                        <div class="tiles info"></div>
                        <div class="tiles dark"></div>
                        <div class="tiles default"></div>
                    </div>
                </div>
            </div>

            <!-- partial -->
            <!-- partial:partials/_sidebar.html -->
            @include('admin.layouts.sidebar')
            <!-- partial -->
            <form action="/profile/update" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="container rounded bg-white mt-5 mb-5">
                    <div class="row">
                        <div class="col-md-5 border-right">
                            <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                                <img id="previewImage" class="rounded-circle mt-5 profile-image" width="150px"
                                    src="{{ $admin->foto
                                        ? asset('FotoUser/' . $admin->foto)
                                        : 'https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock
                                                                -illustration-unknown-person-silhouette-glasses-profile.jpg' }}">

                                <h6>Upload a different photo...</h6>
                                <input type="file" class="text-center center-block file-upload" name="foto"
                                    id="Foto" onchange="previewFile()">
                            </div>
                        </div>
                        <div class="col-md-7 border-right">

                            <div class="p-3 py-5">
                                <div class="d-flex justify-content-between align-items-center mb-3">
                                    <h4 class="text-right">Profile </h4>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-6"><label class="labels">Username</label><input type="text"
                                            class="form-control" placeholder="Username" value="{{ $users->username }}"
                                            readonly></div>
                                    <div class="col-md-6"><label class="labels">Email</label><input type="text"
                                            class="form-control" placeholder="Email"
                                            value="{{ $users->email }}" readonly></div>

                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12"><label class="labels">Nama Lengkap</label><input
                                            type="text" class="form-control" placeholder="first name" name="nama"
                                            value="{{ $admin ? $admin->nama : old('nama') }}"></div>
                                    <div class="col-md-12"><label class="labels">NIK</label><input type="text"
                                            class="form-control" placeholder="masukkan NIK" name="NIK"
                                            value="{{ $admin ? $admin->NIK : old('NIK') }}"></div>
                                    <div class="col-md-12"><label class="labels">Alamat</label><input type="text"
                                            class="form-control" placeholder="masukkan Alamat" name="alamat"
                                            value="{{ $admin ? $admin->alamat : old('alamat') }}"></div>

                                </div>
                                <div class="mt-5 text-center"><button class="btn btn-primary profile-button"
                                        type="submit">Save Profile</button></div>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    @include('admin.layouts.script')
</body>

</html>
