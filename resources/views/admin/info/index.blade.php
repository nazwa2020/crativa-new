<!DOCTYPE html>
<html lang="en">

@include('admin.layouts.head')

@php
    $about1 = $about->where('id_about', '01')->first();
        $about2 = $about->where('id_about', '02')->first();
        $about3 = $about->where('id_about', '03')->first();
        $about4 = $about->where('id_about', '04')->first();
        $about5 = $about->where('id_about', '05')->first();
        $about6 = $about->where('id_about', '06')->first();
@endphp
<body class="with-welcome-text">
    <div class="container-scroller">
        @include('admin.layouts.navbar')
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:partials/_settings-panel.html -->
            <div class="theme-setting-wrapper">
                <div id="settings-trigger"><i class="ti-settings"></i></div>
                <div id="theme-settings" class="settings-panel">
                    <i class="settings-close ti-close"></i>
                    <p class="settings-heading">SIDEBAR SKINS</p>
                    <div class="sidebar-bg-options selected" id="sidebar-light-theme">
                        <div class="img-ss rounded-circle bg-light border me-3"></div>Light
                    </div>
                    <div class="sidebar-bg-options" id="sidebar-dark-theme">
                        <div class="img-ss rounded-circle bg-dark border me-3"></div>Dark
                    </div>
                    <p class="settings-heading mt-2">HEADER SKINS</p>
                    <div class="color-tiles mx-0 px-4">
                        <div class="tiles success"></div>
                        <div class="tiles warning"></div>
                        <div class="tiles danger"></div>
                        <div class="tiles info"></div>
                        <div class="tiles dark"></div>
                        <div class="tiles default"></div>
                    </div>
                </div>
            </div>
           
            <!-- partial -->
            <!-- partial:partials/_sidebar.html -->
            @include('admin.layouts.sidebar')
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="home-tab">
                                <div class="d-sm-flex align-items-center justify-content-between border-bottom">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active ps-0" id="home-tab" data-bs-toggle="tab"
                                                href="#overview" role="tab" aria-controls="overview"
                                                aria-selected="true">Header</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="profile-tab" data-bs-toggle="tab"
                                                href="#audiences" role="tab" aria-selected="false">Text-Only</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="contact-tab" data-bs-toggle="tab"
                                                href="#demographics" role="tab" aria-selected="false">About
                                                Company</a>
                                        </li>
                                    </ul>
                                    <div>
                                        <div class="btn-wrapper">
                                            <a href="#" class="btn btn-otline-dark align-items-center"><i
                                                    class="icon-share"></i> Share</a>
                                            <a href="#" class="btn btn-otline-dark"><i
                                                    class="icon-printer"></i> Print</a>
                                            <a href="#" class="btn btn-primary text-white me-0"><i
                                                    class="icon-download"></i> Export</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content tab-content-basic">
                                    <div class="tab-pane fade show active" id="overview" role="tabpanel"
                                        aria-labelledby="overview">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                    <div class=" d-block py-lg-5 py-3 bg-images border-bottom border-thick border-secondary"
                                                        style="background-image: url({{ asset('assets/img/' . $about1->url) }});">
                                                        <div class="section-have-cover container my-lg-5 my-4 px-lg-5">
                                                            <div class="row align-items-center">
                                                                <div class="col-md-7 mb-lg-5 mb-4 text-white">
                                                                    <h1 class="mb-3 text-xlg">{{ $about1->title }}</h1>
                                                                    <p class="mb-4 font-regular pe-lg-5">
                                                                        {{ $about1->description }}</p>
                                                                    <a href="#"
                                                                        class="btn btn-lg btn-primary px-4">Sign Up <i
                                                                            class="icofont-arrow-right ms-3"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="bg-images-cover"></div>

                                                    </div>
                                                    <a href="{{ route('info.edit', $about1->id_about) }}"
                                                        class="btn btn-info btn-lg px-5 mt-3" data-toggle=""
                                                        style="border-radius: 1px; margin-left: 860px;">Edit</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 mt-5">

                                                    <div id="featuresSection" class="d-block bg-white py-lg-5 py-3">
                                                        <div class="container my-lg-5 my-4 px-lg-5 text-center">
                                                            <div class="font-weight-bold text-lg d-block mb-4">
                                                                {{ $about2->title }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="d-block section-xlg bg-images"
                                                        style="background-image: url({{ asset('assets/img/' . $about2->url) }});">
                                                    </div>

                                                    <a href="{{ route('info.edit', $about2->id_about) }}"
                                                        class="btn btn-info btn-lg px-5 mt-3" data-toggle=""
                                                        style="border-radius: 1px; margin-left: 860px;">Edit</a>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane container" id="audiences" role="tabpanel"
                                        aria-labelledby="audiences">
                                        <div class="row mb-6">
                                            <div class="col-sm-12">
                                                    <div class="d-block bg-dark text-white py-lg-5 py-3">
                                                        <div class="container my-lg-5 my-4 px-lg-5 text-center">
                                                            <div class="d-block text-lg font-thin mb-5">
                                                                {{ $about3->title }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="{{ route('info.edit', $about3->id_about) }}"
                                                        class="btn btn-info btn-lg px-5 mt-3 mb-4" data-toggle=""
                                                        style="border-radius: 1px; margin-left: 820px;">Edit</a>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                    <div class="d-block bg-light py-lg-5 py-3">
                                                        <div class="container my-lg-5 my-4 px-lg-5">
                                                            <div class="row justify-content-center">
                                                                <div class="col-md-7">
                                                                    <div class="d-block text-center">
                                                                        <h1 class="mb-3">{{ $about6->title }}</h1>
                                                                        <p class="font-regular">
                                                                            {{ $about6->description }}</p>
                                                                        <a href="#"
                                                                            class="btn btn-primary btn-lg px-4">Signup
                                                                            Now <i
                                                                                class="icofont-arrow-right ms-3"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="{{ route('info.edit', $about6->id_about) }}"
                                                        class="btn btn-info btn-lg px-5 mt-3" data-toggle=""
                                                        style="border-radius: 1px; margin-left: 820px;">Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane tab-content-basic" id="demographics" role="tabpanel"
                                        aria-labelledby="demographics">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <!-- Section > Content -->
                                                    <div class="d-block py-lg-5 py-3">
                                                        <div class="container my-lg-5 my-4 px-lg-5">
                                                            <div class="row align-items-center">
                                                                <div class="col-md-6 order-md-2 mb-lg-0 mb-4">
                                                                    <div class="block-img rounded"
                                                                        style="background-image: url({{ asset('assets/img/' . $about4->url) }});">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 order-md-1 mb-lg-0 mb-4">
                                                                    <div class="d-block pe-lg-5">
                                                                        <h2 class="mb-3">{{ $about4->title }}</h2>
                                                                        <p class="small m-0">{{ $about4->description }}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="{{ route('info.edit', $about4->id_about) }}"
                                                        class="btn btn-info btn-lg px-5 mt-3 mb-4" data-toggle=""
                                                        style="border-radius: 1px; margin-left: 820px;">Edit</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <!-- Section > Content -->
                                                    <div class="d-block py-lg-5">
                                                        <div class="container my-lg-5 my-4 px-lg-5">
                                                            <div class="row align-items-center">
                                                                <div class="col-md-6 mb-lg-0 mb-3">
                                                                    <div class="block-img rounded"
                                                                        style="background-image: url({{ asset('assets/img/' . $about5->url) }});">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 mb-lg-0 mb-3">
                                                                    <div class="d-block ps-lg-5">
                                                                        <h2 class="mb-3">{{ $about5->title }}</h2>
                                                                        <p class="small m-0">{{ $about5->description }}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="{{ route('info.edit', $about5->id_about) }}"
                                                        class="btn btn-info btn-lg px-5 mt-3" data-toggle=""
                                                        style="border-radius: 1px; margin-left: 820px;">Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content-wrapper ends -->
                @include('admin.layouts.footer')
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    @include('admin.layouts.script')
</body>

</html>
