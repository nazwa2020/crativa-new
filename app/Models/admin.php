<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class admin extends Model
{
    use HasFactory;
    protected $table = 'admin';
    protected $fillable = ['id_admin', 'id_users', 'nama', 'NIK', 'no_telp', 'alamat', 'foto'];
    protected $primaryKey = 'id_admin';
}
