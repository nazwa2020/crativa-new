<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class privillege extends Model
{
    use HasFactory;
    protected $table = 'privillege';
    protected $fillable = ['id_pricing', 'privillege'];

    public function pricing() {
        return $this->belongsTo(pricing::class, 'id_pricing', 'id_pricing');
    }
}
