<?php

namespace Database\Seeders;

use App\Models\pricing;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PricingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        pricing::create([
            'id_pricing' => 'PR01',
            'categories' => 'Intro',
            'price' => '$5/month',
            'feature' => 'Up to 4 connection'
        ]);

        pricing::create([
            'id_pricing' => 'PR02',
            'categories' => 'Medium',
            'price' => '$15/month',
            'feature' => 'Up to 6 connection, Free installation, Special meals every 1 week'
        ]);

        pricing::create([
            'id_pricing' => 'PR03',
            'categories' => 'Well Done',
            'price' => '$40/month',
            'feature' => 'Up to 12 connection, Free installation, Special people every 1 week, Free futurism'
        ]);
    }
}
