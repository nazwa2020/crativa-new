<div class="d-block bg-primary text-white py-lg-5 py-3">
    <div class="container px-lg-5">
        <div class="d-flex d-flex-mobile align-items-start justify-content-center">
            @foreach ($contact as $item)
            <div class="mx-3 text-center mb-lg-0 mb-3">
                <small class="font-weight-bold"><i class="{{$item->icon}}"></i>{{$item->contact_cat}}</small>
                <div class="d-block font-weight-bold">{{$item->information}}</div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<!-- Navigation -->
<div class="nav-top bg-primary px-lg-4 px-2 pb-5 pt-5">
    <div class="nav-inner h-100">
        <div class="container-fluid h-100">
            <div class="row h-100">
                <div class="col-4 h-100">
                    <div class="d-flex align-items-center h-100">
                        <div class="nav-top-menus nav-top-menus--white">
                            <a href="#featuresSection" class="menus-link me-3">Features</a>
                            <a href="#pricingSection" class="menus-link me-3">Pricing</a>
                            <a href="#contactSection" class="menus-link">Contact</a>
                        </div>
                    </div>
                </div>
                <div class="col-4 h-100">
                    <div class="d-flex justify-content-center align-items-center h-100">
                        <a href="#" class="nav-logo nav-logo-lg text-white">
                            Crativa
                        </a>
                    </div>
                </div>
                <div class="col-4 h-100">
                    <div class="d-flex align-items-center justify-content-end h-100">
                        <div class="nav-top-menus nav-top-menus--white">
                            <a href="#" class="menus-link me-3"><i class="icofont-instagram"></i></a>
                            <a href="#" class="menus-link"><i class="icofont-twitter"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>