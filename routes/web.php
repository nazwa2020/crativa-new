<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\FeatureController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\PricingController;
use App\Http\Controllers\PrivillegeController;
use App\Http\Controllers\ReadUpdateController;
use App\Http\Middleware\AlreadyLogin;
use App\Http\Middleware\Guest;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [IndexController::class, 'index'])->name('dashboard');

Route::middleware([Guest::class])->group(function () {

    Route::get('/register', [AdminController::class, 'register'])->name('/register');
    Route::post('/register/store', [AdminController::class, 'store'])->name('/register/store');
    Route::get('/login', [AdminController::class, 'login'])->name('/login');
    Route::post('/login/action', [AdminController::class, 'action'])->name('/login/action');

});

Route::middleware([AlreadyLogin::class])->group(function () {

    Route::get('/admin', [AdminController::class, 'main'])->name('/admin');
    Route::get('/profile', [AdminController::class, 'profile'])->name('/profile');
    Route::post('/profile/update', [AdminController::class, 'update'])->name('/profile/update');

    Route::group(['prefix' => 'feature', 'as' => 'feature.'], function () {
        Route::get('/', [FeatureController::class, 'feature'])->name('feature');
        Route::get('/add', [FeatureController::class, 'add'])->name('add');
        Route::post('/store', [FeatureController::class, 'store'])->name('store');
        Route::get('/edit/{feature}', [FeatureController::class, 'edit'])->name('edit');
        Route::post('/update/{feature}', [FeatureController::class, 'update'])->name('update');
        Route::get('/delete/{feature}', [FeatureController::class, 'delete'])->name('delete');
    });

    Route::group(['prefix' => 'pricing', 'as' => 'pricing.'], function () {
        Route::get('/', [PricingController::class, 'pricing'])->name('pricing');
        Route::get('/add', [PricingController::class, 'add'])->name('add');
        
        Route::post('/store', [PricingController::class, 'store'])->name('store');
        Route::get('/edit/{pricing}', [PricingController::class, 'edit'])->name('edit');
        Route::post('/update/{pricing}', [PricingController::class, 'update'])->name('update');
        Route::get('/delete/{pricing}', [PricingController::class, 'delete'])->name('delete');
    });
        Route::get('/privillege', [PrivillegeController::class, 'privillege'])->name('privillege');
        Route::get('/addPrivillege/{id_pricing}', [PricingController::class, 'addPrivillege'])->name('addPrivillege/{id_pricing}');
        Route::get('/editPrivillege/{id}', [PrivillegeController::class, 'editPrivillege'])->name('editPrivillege/{id}');
        Route::post('/storePrivillege', [PricingController::class, 'storePrivillege'])->name('storePrivillege');
        Route::post('/updatePrivillege/{id}', [PrivillegeController::class, 'updatePrivillege'])->name('updatePrivillege/{id}');
        Route::get('/delete/{id}', [PrivillegeController::class, 'delete'])->name('delete/{id}');

    Route::group(['prefix' => 'info', 'as' => 'info.'], function () {
        Route::get('/', [ReadUpdateController::class, 'coba'])->name('info');
        Route::get('/edit/{info}', [ReadUpdateController::class, 'editInfo'])->name('edit');
        Route::post('/update/{info}', [ReadUpdateController::class, 'updateInfo'])->name('update');
    });

    Route::group(['prefix' => 'contact', 'as' => 'contact.'], function () {
        Route::get('/', [ReadUpdateController::class, 'contact'])->name('contact');
        Route::get('/edit/{contact}', [ReadUpdateController::class, 'editContact'])->name('edit');
        Route::post('/update/{contact}', [ReadUpdateController::class, 'updateContact'])->name('update');
    });
});

route::get('/logout', [AdminController::class, 'logout']);
