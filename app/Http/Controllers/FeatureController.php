<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\features;
use Illuminate\Http\Request;

class FeatureController extends Controller
{
    public function feature()
    {
        $feature = features::get();
        return view('admin.feature.index', compact('feature'));
    }

    public function add()
    {
        $feature = null;
        return view("admin.feature.action", compact('feature'));
    }

    public function edit(features $feature)
    {
        $feature = $feature;
        return view("admin.feature.action", compact('feature'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'id_feature' => 'required|unique:feature,id_feature',
            'title' => 'required',
            'information' => 'required'
        ], [
            'id_feature.required' => 'ID feature harus diisi.',
            'id_feature.unique' => 'ID feature has been used.',
            'title.required' => 'Judul feature harus diisi.',
            'information.required' => 'Informasi feature harus diisi.'
        ]);

        features::create([
            'id_feature' => $request->id_feature,
            'title' => $request->title,
            'information' => $request->information
        ]);

        // return redirect()->route('feature.feature');
        // Dalam kontroler Anda
        return redirect()->route('feature.feature')->with('success', 'Data berhasil ditambahkan!');

    }

    public function update(Request $request, features $feature)
    {
        $request->validate([
            'id_feature' => 'required',
            'title' => 'required',
            'information' => 'required'
        ], [
            'id_feature.required' => 'ID feature harus diisi.',
            'title.required' => 'Judul feature harus diisi.',
            'information.required' => 'Informasi feature harus diisi.'
        ]);

        $feature->update([
            'id_feature' => $request->id_feature,
            'title' => $request->title,
            'information' => $request->information
        ]);

        // return redirect()->route('feature.feature');
        return redirect()->route('feature.feature')->with('success', 'Data berhasil diubah!');

    }

    public function delete (features $feature)
    {
            // $feature parameter sudah berisi instansiasi snackbox berdasarkan id_feature
    
        if (!$feature) {
            // Jika feature tidak ditemukan, mungkin hendak menampilkan pesan error atau melakukan tindakan lain
            return redirect('/feature')->with('error', 'feature not found');
        }
    
        // Hapus record feature dari database
        $feature->delete();
    
        // Redirect ke halaman yang sesuai atau tampilkan pesan sukses
        return redirect('/feature')->with('success', 'feature deleted successfully');
    }
}