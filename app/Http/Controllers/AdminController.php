<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\admin;
use App\Models\users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function main()
    {
        return view('admin.main');
    }

    public function profile()
    {
        $users = users::where('id', Auth::user()->id)->first();
        $admin = admin::where('id_user', Auth::user()->id)->first();
        // dd($admin);
        return view('admin.profile', compact('users', 'admin'));
    }

    public function update(Request $request)
    {
        $create_nama_foto = 'a';
        if ($request->hasFile('foto')) {
            // $request->validate([
            //     'foto' => 'image|mimes:jpeg,png,jpg|max:5160',
            // ], [
            //     'foto.mimes' => 'foto harus berbentuk jpeg, jpg, png.',
            //     'foto.max' => 'Ukuran foto tidak boleh melebihi 5 MB.',
            // ]);

            $foto = $request->file('foto');
            $create_nama_foto = time() . '.' . $foto->extension();
            $move_foto = $foto->move(public_path('FotoUser'), $create_nama_foto);

            admin::where('id_user', Auth::user()->id)->update(
                [
                    'foto' => $create_nama_foto
                ]
            );
        }

        admin::where('id_user', Auth::user()->id)->update(
            [
                'nama' => $request->nama,
                'no_telp' => $request->no_telp,
                'NIK' => $request->NIK,
                'alamat' => $request->alamat
            ]
        );

        // dd($request->all());
        return redirect('/profile');
    }

    public function form()
    {
        return view('admin.forms.basic_elements');
    }

    public function register()
    {
        return view('login.register');
    }

    public function login()
    {
        return view('login.login');
    }

    public function store(Request $request)
    {
        Session::flash('username', $request->username);
        Session::flash('email', $request->email);

        $request->validate([
            'password' => 'required|min:8|regex:/^(?=.*[0-9])(?=.*[a-zA-Z])/',
            'email' => 'required|unique:users,email',
            'username' => 'required|unique:users,username'
        ], [
            'email.unique' => 'email sudah digunakan.',
            'username.unique' => 'username sudah digunakan.'
        ]);

        users::insert([
            'id'       => $request->id,
            'username' => $request->username,
            'password' => password_hash($request->password, PASSWORD_DEFAULT),
            'email'    => $request->email
        ]);

        admin::insert([
            'id_admin'       => $request->id,
            'id_user'        => users::max('id')
        ]);

        return redirect('/login')->with('success', 'Registrasi Admin Telah berhasil');
    }

    public function action(Request $request)
    {
        Session::flash('email', $request->email);

        $data = [
            'email' => $request->input('email'),
            'password' => $request->password,
        ];

        if (Auth::attempt($data)) {
            $request->session()->regenerate();
            return redirect()->intended('/admin');
        } else {
            // Add an alert message for incorrect username or password
            return redirect('/login')->with('error', 'Email atau password tidak valid. Silakan coba lagi.');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
